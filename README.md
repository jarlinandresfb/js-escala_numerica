![Escala Numérica](http://www.madarme.co/portada-web.png)

# Título del proyecto: Escala numérica

En este proyecto se realiza un algoritmo que hace el cálculo de una escala numérica de acuerdo a una entrada de un número que se le proporcione.

# Tabla de Contenido 
1. [Escala Numérica](#título-del-proyecto-Escala-numérica)
2. [Ejemplo](#ejemplo)
3. [Características](#características)
4. [Tecnologías](#tecnologías)
5. [IDE](#ide)
6. [Demo](#demo)
7. [Contribución](#contribución)
8. [Autor](#autor)
9. [Institución Educativa](#institución-educativa)

## Ejemplo
A continuacion ejemplo del funcionamiento de una Escala Numerica.
Se muestra tabla con datos referentes Escala del Numero : 1
 
> Operación Multiplicar

| Dato 1 | Dato 2 | Resultado |
| - | -| - |
| 1	| 1 | 1 |
| 1	| 2	| 2 |
| 2 | 3 | 6 |
| 6 |	4 |	24 |
| 24 | 5 	| 120 |
| 120 |	6 |	720 |
| 720 |	7 |	5040 |
| 5040 |	8 |	40320 |
| 40320 |	9 |	362880 |
| 362880 |	10 |	3628800 |

> Operación Dividir 

| Dato 1 | Dato 2 | Resultado |
|-|-|-|
| 3628800	| 1	| 3628800 |
| 3628800	| 2	| 1814400 |
| 1814400	| 3	| 604800 |
| 604800	| 4	| 151200 |
| 151200	| 5 	| 30240 |
| 30240	| 6	| 5040 |
| 5040	| 7	| 720 |
| 720	| 8	| 90 |
| 90	| 9	| 10 |
| 10	| 10 	| 1 |


## Características




## Contenido del proyecto 

* [Contenido del JavaScript](js/funciones.js)

* [Contenido del index CSS](css/index.css)

* [Contenido del Menu](css/menu.css) 


## Tecnologías
+ HTML5
+ CCS
+ JavaScript

Usted puede encontrar más información acerca de HTML5, CSS y JavaScript en los siguientes enlaces: 

* La [Guía de JavaScript](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide "Guía de JavaScript")  te muestra cómo usar [JavaScript](https://www.w3schools.com/js/default.asp) y te brinda una perspectiva general del lenguaje. 
* Si necesitas información exhaustiva sobre una característica del lenguaje, consulta la [Referencia de JavaScript](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia) 
- Aprendizaje fácil con [HTML](https://www.w3schools.com/html/) "Pruébalo tú mismo"
+ Este [tutorial de CSS](https://www.w3schools.com/css/default.asp) contiene cientos de ejemplos de CSS.

## IDE
* El proyecto se desarrolló usando Sublime Text 3.

## Demo
Este proyecto ha sido despleago en el servidor madarme.co. Para ver el demo de la aplicación puede dirigirse a: 
[Escala numerica](http://ufps16.madarme.co/js-escala_numerica/)

## Contribución 
* Jarlin Andres Fonseca Bermón

## Autor
Proyecto desarrollado por [Marco Adarme] (<madarme@ufps.edu.co>).

Proyecto editado por [Jarlin Fonseca] (<jarlinandresfb@ufps.edu.co>)

## Institución Académica   
Proyecto desarrollado en la Materia programación web del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]



